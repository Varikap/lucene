﻿Đelić: Kandidati za godinu dana
22. novembar 2009. | 14:54 -> 20:40 | Izvor: FoNet, Beta
Kruševac, Leskovac -- Ako u decembru bude odblokiran Prelazni trgovinski sporazum, Srbija ima šanse da do statusa kanditata za EU stigne za godinu dana, smatra Božidar Đelić.
"Odluka ministara EU da se odblokira prelazni trgovinski sporazum, pokazala bi da postoji politička volja da potencijalni zahtev Srbije za članstvo u EU bude pozitivno primljen“, rekao je potpredsednik Vlade Božidar Đelić u Kruševcu na tribini "Evropa za sve", posvećenoj promociji stavljanja Srbije na "belu šengensku listu".

Teme
Evroatlantske integracije

    * Leskovac: Projekat "Evropa za sve" | 22. novembar | 14:12
    * Mirel: BiH nije ispunila uslove | 22. novembar | 14:04
    * Kacin: Kandidatura nerealna | 22. novembar | 01:34
    * SPO: Ohrabrujuće najave Holandije | 21. novembar | 13:23
    * FPN: Pozitivan stav o Srbiji | 21. novembar | 01:41
    * Usvojena rezolucija LDP-a | 20. novembar | 15:28
    * Miščevićeva: Šansa za Srbiju | 20. novembar | 15:03
    * Britanski mediji o novom vrhu EU | 20. novembar | 10:58
    * Van Rompej za proširenje EU | 20. novembar | 10:44
    * Kandidatura polovinom decembra? | 20. novembar | 09:10
    * Prikaži više vesti iz ove teme
    * Prikaži manje vesti iz ove teme
    * sve vesti iz teme "Evroatlantske integracije"

“Moja procena je, imajući u vidu da je Evropska komisija u izveštaju u oktobru priznala Srbiji sve što smo uradili od januara do sada, da bi put od podnošenja zahteva za članstvo do formalnog dobijanja statusa kandidata, mogao da bude kratak, možda samo godinu dana", ocenio je Đelić.

Potpredsednik Vlade je ranije danas, prilikom posete Leskovcu gde je takođe održana tribina o ukidanju viza za građane Srbije, rekao da postoje šanse da Srbija u 2014. godini postane članica Evropske unije.

"Godina 2014. je, za sada, najraniji mogući način za ulazak u EU. Jeste mnogo tesno, ali nije nemoguće. Ako do 2012. godine budemo kompletno usklađeni sa zakonima EU, ulazimo tu negde, posle Hrvatske", istakao je Đelić, podsetivši na inicijativu Grčke da preostale bakanske zemlje uđu u EU u 2014. godini, na godišnjicu početka Prvog svetskog rata.

"Ima mnogo ljudi koji su skeptični oko toga, ali ja mislim da Srbija ima velike šanse", dodao je Đelić.

“Činjenice umesto stereotipa“

Tokom posete Leskovcu, Đelić nije želeo da komentariše kritike izvestioca Evropskog parlamenta za Srbiju Jelka Kacina na račun vlasti u Beogradu, napominjući da je jedino merodavno šta će Kacin napisati u svom izveštaju, što će budno biti praćeno.

"Mi ćemo budno to pratiti i zahtevati da se u izveštaju nađu isključivo činjenice, a ne stereotipi i neki drugi aspekti za koje nema mesta u zavničnom dokumentu Evropskog parlamenta", objasnio je Đelić u izjavi FoNetu.

Pozivajući se na nedavne razgovore u Evropskom parlamentu, Đelić je istakao da je "podrška koju Srbija uživa u ovom domu veoma visoka".

"Ako vam je za to potreban neki dokaz, neka to bude prošlonedeljno glasanje u Evropskom parlamentu za viznu liberalizaciju naše zemlje, kada je 86 odsto poslanika podržalo Srbiju", zaključio je Đelić.